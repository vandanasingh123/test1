from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView,View
from .models import Contact
from .forms import MyModelForm
from django.views.generic.edit import FormView

from django.urls import reverse_lazy
# Create your views here.


class ContactList(ListView):
    model = Contact

class ContactDetail(DetailView):
    model = Contact



class ContactCreate(CreateView):
    model = Contact
    # form_class = MyModelForm
    fields = '__all__'
    # form_class = ContactForm
    template_name = 'polls/contact_form.html'
    success_url = reverse_lazy('contact_list')

class ContactUpdate(UpdateView):
    model = Contact
    fields = '__all__'
    # form_class = ContactForm
    success_url = reverse_lazy('contact_list')

class ContactDelete(DeleteView):
    model = Contact
    success_url = reverse_lazy('contact_list')

from django.shortcuts import render

# Create your views here.
