from django.db import models

# Create your models here.
from django.core.validators import RegexValidator

from django.contrib.gis.db import models



GENDER_CHOICES = (
    ('male','Male'),
    ('female', 'FEMALE'),

)


class Contact(models.Model):
    Name = models.CharField(max_length=100, default=0)
    Pan_number = models.TextField(max_length=100, default=0)
    Age = models.IntegerField(max_length=100,default=0)
    Gender = models.TextField(max_length=6, choices=GENDER_CHOICES)
    email = models.EmailField()
    City = models.TextField(max_length=100, default=0)

    def __str__(self):
        return self.Name


from django.db import models

# Create your models here.
