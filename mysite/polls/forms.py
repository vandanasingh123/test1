
from .models import Contact
from django import forms
from django.core.validators import validate_email

class MyModelForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['Gender']

